function ajaxCall(method, url, data, before, done, fail) {
    method = typeof method !== 'undefined' ? method : 'GET';
    $.ajax({
        type: method,
        url: url,
        data: data,
        beforeSend: before,
        error: function (response) {
//            unblockUI();
        }
    }).done(done).fail(fail);
}

function ajaxGet(url,before, done, fail) {
    ajaxCall('GET', url, undefined, before, done, fail);
}
function ajaxPost(url, data, before, done, fail) {
    ajaxCall('POST', url, data, before, done, fail);
}

function blockUI(message){
    $.blockUI(
        {
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            },
            message: message
        }
    );
}

function unblockUI(message){
    if(message == undefined)
        $.unblockUI();
    else{
        blockUI(message);
        setTimeout($.unblockUI, 1000);
    }
}