# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('testApp', '0002_tag'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tag',
            name='products',
        ),
        migrations.AddField(
            model_name='product',
            name='tags',
            field=models.ManyToManyField(to='testApp.Tag'),
        ),
    ]
