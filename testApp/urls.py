__author__ = 'muhammadshaker'

from django.conf.urls import url

from testApp.views import index, product, tag


urlpatterns = [
    url(r'^$', index.index, name='home_page'),

    url(r'^product/?$', product.index, name='product_index'),
    url(r'^product/(?P<product_id>[0-9]+)$', product.view, name='product_view'),
    url(r'^product/create/?$', product.create, name='product_create'),
    url(r'^product/(?P<product_id>[0-9]+)/update$', product.update, name='product_update'),
    url(r'^product/(?P<product_id>[0-9]+)/delete$', product.delete, name='product_delete'),

    url(r'^tag/?$', tag.index),
    url(r'^tag/search/?$', tag.search, name='tag_search'),


]

