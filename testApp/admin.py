from django.contrib import admin

# Register your models here.
from .models import Product, Tag


class ProductAdmin(admin.ModelAdmin):
    pass


class TagAdmin(admin.ModelAdmin):
    pass

admin.site.register(Product, ProductAdmin)
admin.site.register(Tag, TagAdmin)