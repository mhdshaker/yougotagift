__author__ = 'muhammadshaker'

from django.forms import ModelForm
from testApp.models import Tag, Product


class ProductForm(ModelForm):
    class Meta:
        model = Product
        fields = ['name', 'tags']


class TagForm(ModelForm):
    class Meta:
        model = Tag
        fields = ['name']


