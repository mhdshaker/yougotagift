__author__ = 'muhammadshaker'

from models import Product, Tag


def get_products(limit=None, **filters):
    if limit:
        return Product.objects.filter(**filters)[:limit]
    return Product.objects.filter(**filters)


def create_product(name, tags):
    product = Product()
    product.name = name
    product.save()
    tags = tags.split(";")
    existing_tags = Tag.objects.filter(name__in=tags)
    for existing_tag in existing_tags:
        tags.remove(existing_tag.name)
        product.tags.add(existing_tag)

    for tag in tags:
        tag_new = Tag()
        tag_new.name = tag
        tag_new.save()
        product.tags.add(tag_new)

    product.save()
    return product




