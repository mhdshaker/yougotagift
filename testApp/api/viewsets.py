from django.contrib.auth.models import User
from rest_framework import viewsets
from testApp.api.serializers import UserSerializer, ProductSerializer
from testApp.models import Product

__author__ = 'muhammadshaker'


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
