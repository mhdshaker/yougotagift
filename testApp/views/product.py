from testApp.forms import ProductForm
from testApp.models import Product

__author__ = 'muhammadshaker'


from django.shortcuts import render, redirect
from django.http import HttpResponse
from testApp.services import get_products, create_product


# Create your views here.
def index(request):
    products = get_products()
    return render(request, 'product/index.html', {'products': products})


def view(request, product_id):
    product = Product.objects.get(pk=product_id)
    return render(request, 'product/view.html', {'product': product})


def create(request):
    if request.method == 'POST':
        form = ProductForm(request.POST)
        #TODO: validation
        product = create_product(request.POST['name'], request.POST['tags'])
        if product:
            return redirect('testApp:product_view', product_id=product.id)
    else:
        form = ProductForm()
    return render(request, 'product/create.html', {'form': form})


def update(request, product_id):
    return HttpResponse("update product"+product_id)


def delete(request, product_id):
    return HttpResponse("delete product" + product_id)








