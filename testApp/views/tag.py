import json
from django.core import serializers
from testApp.models import Tag

__author__ = 'muhammadshaker'
from django.shortcuts import render
from django.http import HttpResponse


# Create your views here.
def index(request):
    return HttpResponse("Hello world.")


# Create your views here.
def search(request):
    tags = Tag.objects.filter(name__contains=request.GET['tag_name'])
    tags_list = [tag.name for tag in tags]
    return HttpResponse(json.dumps(tags_list), content_type="application/json")

